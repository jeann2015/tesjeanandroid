package com.hntestmobile.mytestjean.connection

import android.os.StrictMode
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONObject
import java.io.IOException

class Api {
    private var url = "https://hn.algolia.com/api/v1/search_by_date?query=mobile"

    fun connect(): Any {

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        val client = OkHttpClient()

        val request = Request.Builder()
                .url("https://hn.algolia.com/api/v1/search_by_date?query=mobile")
                .build()

        try {

            client.newCall(request).execute().use { response -> if (!response.isSuccessful) throw IOException("Unexpected code $response")
                val r = response.body!!.string()
                val jsonSuccess = JSONObject(r)
                return JSONObject(jsonSuccess.toString())
            }

        } catch (e: Exception) {
            return "error,$e"
        }
    }


}