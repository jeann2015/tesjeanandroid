package com.hntestmobile.mytestjean.activities

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.hntestmobile.mytestjean.R
import com.hntestmobile.mytestjean.adapter.MovieModel
import com.hntestmobile.mytestjean.adapter.MoviesAdapter
import com.hntestmobile.mytestjean.connection.Api
import com.hntestmobile.mytestjean.db.DbManager
import org.json.JSONObject
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {
    private val connect = Api();
    private var dbManager: DbManager? = null
    private val movieList = ArrayList<MovieModel>()
    private lateinit var moviesAdapter: MoviesAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        lateinit var swipeRefreshLayout: SwipeRefreshLayout
        var number: Int = 0

        this.refreshEndPoint();
        swipeRefreshLayout = findViewById(R.id.swipe)
        swipeRefreshLayout.setOnRefreshListener {
            number++
            Handler().postDelayed(Runnable {
                swipeRefreshLayout.isRefreshing = false
                this.refresh();
            }, 2000)
        }
    }
    private fun refreshEndPoint() {
        val authorsList = JSONObject(connect.connect().toString())
        val items = authorsList.getJSONArray("hits")
        var values = ContentValues()
        this.dbManager = DbManager(this)
        val selectionArgs = arrayOf('1'.toString())
        this.dbManager?.Delete("id >= ?", selectionArgs, "authors")

        for (i in 0 until items.length()) {
            this.dbManager = DbManager(this)
            val item = items.getJSONObject(i)

            if(item["story_title"].toString() != ""){
                values.put("story_title", item["story_title"].toString())
                values.put("author", item["author"].toString())
                values.put("created_at", item["created_at"].toString())
                this.dbManager?.Insert(values, "authors");
            } else if(item["title"].toString() != ""){
                values.put("story_title", item["title"].toString())
                values.put("author", item["author"].toString())
                values.put("created_at", item["created_at"].toString())
                this.dbManager?.Insert(values, "authors");
            }

        }

        val recyclerView: RecyclerView = findViewById(R.id.recyclerView)
        moviesAdapter = MoviesAdapter(movieList)
        val layoutManager = LinearLayoutManager(applicationContext)
        recyclerView.layoutManager = layoutManager
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.adapter = moviesAdapter

        val projections = arrayOf(
            "id",
            "story_title",
            "author",
            "created_at"
        );

        this.dbManager = DbManager(this)

        val cursor = this.dbManager?.Query(
            projections, null, null, "created_at desc, id ", "authors"
        )


        var storyTitle: String
        var author: String
        var createdAt: String
        var ID: Int

        if (movieList.count() > 0) { movieList.clear(); }

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    storyTitle = cursor.getString(cursor.getColumnIndex("story_title"))
                    author = cursor.getString(cursor.getColumnIndex("author"))
                    createdAt = this.formatTimeAgo(cursor.getString(cursor.getColumnIndex("created_at")))
                    ID = cursor.getInt(cursor.getColumnIndex("id"))
                    var movie = MovieModel(storyTitle, author, createdAt,ID)
                    movieList.add(movie)
                } while (cursor.moveToNext());
            }
            cursor.close();

            val swipeDelete = object : SwipeToDeleteCallback(this@MainActivity){
                override fun onSwiped(
                    viewHolder: RecyclerView.ViewHolder,
                    direction: Int
                ) {
                    moviesAdapter.deleteItem(viewHolder.adapterPosition)
                }
            }

            val touchHelper  = ItemTouchHelper(swipeDelete)
            touchHelper.attachToRecyclerView(recyclerView)

        }
    }

    fun viewDetails(view: View){
        val IdAuthor: TextView = findViewById(R.id.ID)
        val intent = Intent(this, ShowDetails::class.java)
        intent.putExtra("authorId", IdAuthor.text)
        startActivity(intent)
    }


    private fun refresh() {

        val recyclerView: RecyclerView = findViewById(R.id.recyclerView)
        moviesAdapter = MoviesAdapter(movieList)
        val layoutManager = LinearLayoutManager(applicationContext)
        recyclerView.layoutManager = layoutManager
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.adapter = moviesAdapter

        val projections = arrayOf(
                "id",
                "story_title",
                "author",
                "created_at"
        );

        this.dbManager = DbManager(this)

        val cursor = this.dbManager?.Query(
                projections, null, null, "created_at desc, id ", "authors"
        )

        var storyTitle: String
        var author: String
        var createdAt: String
        var ID: Int

         if (movieList.count() > 0) { movieList.clear() }

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    storyTitle = cursor.getString(cursor.getColumnIndex("story_title"))
                    author = cursor.getString(cursor.getColumnIndex("author"))
                    createdAt = this.formatTimeAgo(cursor.getString(cursor.getColumnIndex("created_at")))
                    ID = cursor.getInt(cursor.getColumnIndex("id"))
                    var movie = MovieModel(storyTitle, author, createdAt,ID)
                    movieList.add(movie)
                } while (cursor.moveToNext());
            }
            cursor.close();

            val swipeDelete = object : SwipeToDeleteCallback(this@MainActivity){
                override fun onSwiped(
                    viewHolder: RecyclerView.ViewHolder,
                    direction: Int,
                ) {
                    moviesAdapter.deleteItem(viewHolder.adapterPosition)
                }
            }

            val touchHelper  = ItemTouchHelper(swipeDelete)
            touchHelper.attachToRecyclerView(recyclerView)

        }
    }
    private fun formatTimeAgo(date1: String): String {
        var conversionTime =""

        try{
            val format = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

            val sdf = SimpleDateFormat(format)
            sdf.timeZone = TimeZone.getTimeZone("GMT")

            val datetime= Calendar.getInstance()
            val date2= sdf.format(datetime.time).toString()

            val Past = sdf.parse(date1)
            val Now = sdf.parse(date2)
            val diff = Now.time - Past.time

            val diffsec = diff  / 1000
            val diffmin = diff / (60 * 1000)
            val diffhours = diff / (60 * 60 * 1000)
            val diffDays = diff / (24 * 60 * 60 * 1000)
            val diffMonts = diff / (30 * 24 * 60 * 60 * 1000)
            val diffYears = diff / (24 * 365 * 60 * 60 * 1000)

            when {
                diff<diffYears -> {
                    conversionTime+= " $diffYears years ago"
                }
                diffMonts>1 -> {
                    conversionTime+= (diffMonts-diffYears/12).toString()+" months ago "
                }
                diffDays>1 -> {
                    conversionTime += (diffDays-diffMonts/30).toString()+" days ago "
                }
                diffhours>1 -> {
                    conversionTime+=(diffhours-diffDays*24).toString()+" hours ago "
                }
                diffmin>1 -> {
                    conversionTime+=(diffmin-diffhours*60).toString()+" min ago "
                }
                diffsec>1 -> {
                    conversionTime+= " Just Now "
                }
            }

        }catch (ex:java.lang.Exception){
            Log.e("formatTimeAgo",ex.toString())
        }
        return conversionTime
    }
}
