package com.hntestmobile.mytestjean.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.hntestmobile.mytestjean.R
import com.hntestmobile.mytestjean.adapter.MovieModel
import com.hntestmobile.mytestjean.db.DbManager
import java.text.SimpleDateFormat
import java.util.*

class ShowDetails : AppCompatActivity() {
    private var authorId: String = "";
    private var dbManager: DbManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_details)

        this.authorId = intent.getStringExtra("authorId").toString();

        println(this.authorId)

        val projections = arrayOf(
            "id",
            "story_title",
            "author",
            "created_at"
        );
        val selectionArgs = arrayOf(this.authorId)

        this.dbManager = DbManager(this)

        val cursor = this.dbManager?.Query(
            projections, "id=?", selectionArgs, "created_at desc, id ", "authors"
        )


        var storyTitle: String = ""
        var author: String =  ""
        var createdAt: String = ""
        var ID: Int

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    storyTitle = cursor.getString(cursor.getColumnIndex("story_title"))
                    author = cursor.getString(cursor.getColumnIndex("author"))
                    createdAt = this.formatTimeAgo(cursor.getString(cursor.getColumnIndex("created_at")))
                    ID = cursor.getInt(cursor.getColumnIndex("id"))
                } while (cursor.moveToNext());
            }
            cursor.close();

            val title: TextView = findViewById(R.id.title)
            title.text = storyTitle

            val year: TextView = findViewById(R.id.year)
            year.text = createdAt

            val genre: TextView = findViewById(R.id.genre)
            genre.text = author

        }



    }

    fun back(view: View){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    private fun formatTimeAgo(date1: String): String {
        var conversionTime =""

        try{
            val format = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

            val sdf = SimpleDateFormat(format)
            sdf.timeZone = TimeZone.getTimeZone("GMT")

            val datetime= Calendar.getInstance()
            val date2= sdf.format(datetime.time).toString()

            val Past = sdf.parse(date1)
            val Now = sdf.parse(date2)
            val diff = Now.time - Past.time

            val diffsec = diff  / 1000
            val diffmin = diff / (60 * 1000)
            val diffhours = diff / (60 * 60 * 1000)
            val diffDays = diff / (24 * 60 * 60 * 1000)
            val diffMonts = diff / (30 * 24 * 60 * 60 * 1000)
            val diffYears = diff / (24 * 365 * 60 * 60 * 1000)

            when {
                diff<diffYears -> {
                    conversionTime+= " $diffYears years ago"
                }
                diffMonts>1 -> {
                    conversionTime+= (diffMonts-diffYears/12).toString()+" months ago "
                }
                diffDays>1 -> {
                    conversionTime += (diffDays-diffMonts/30).toString()+" days ago "
                }
                diffhours>1 -> {
                    conversionTime+=(diffhours-diffDays*24).toString()+" hours ago "
                }
                diffmin>1 -> {
                    conversionTime+=(diffmin-diffhours*60).toString()+" min ago "
                }
                diffsec>1 -> {
                    conversionTime+= " Just Now "
                }
            }

        }catch (ex:java.lang.Exception){
            Log.e("formatTimeAgo",ex.toString())
        }
        return conversionTime
    }
}