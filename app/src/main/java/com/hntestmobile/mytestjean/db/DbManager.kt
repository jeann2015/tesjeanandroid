package com.hntestmobile.mytestjean.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.database.sqlite.SQLiteQueryBuilder
import android.widget.Toast

/**
 * @author Jean Carlos Nunez
 */
class  DbManager{

    val dbName="db3"
    private val dbTableAuthors="authors"

    val id = "id"
    //authors
    private val storyTitle = "story_title"
    private val author = "author"
    private val createdAt = "created_at"
    //authors

    val dbVersion = 1

    val sqlCreateTableAuthors="CREATE TABLE IF NOT EXISTS "+ dbTableAuthors +" ("+ id +" INTEGER PRIMARY KEY,"+
            storyTitle+" TEXT,"+author+" TEXT,"+createdAt+" TEXT);"

    private var sqlDB:SQLiteDatabase? = null

    constructor(context:Context){
        var db = DatabaseHelperNotes(context)
        sqlDB = db.writableDatabase
    }

    inner class  DatabaseHelperNotes:SQLiteOpenHelper{
         var context:Context?=null
        constructor(context:Context):super(context,dbName,null,dbVersion){
            this.context=context
        }

        override fun onCreate(p0: SQLiteDatabase?) {
            p0!!.execSQL(sqlCreateTableAuthors)
            Toast.makeText(this.context," database is created", Toast.LENGTH_LONG).show()
        }

        override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
//            p0!!.execSQL("Drop table IF EXISTS $dbTablePunch")
//            Toast.makeText(this.context," table is created $dbTablePunch", Toast.LENGTH_LONG).show()
        }

    }

    fun Insert(values:ContentValues, table: String):Long{
        val ID= sqlDB!!.insert(table,"",values)
        sqlDB!!.close();
        return ID
    }

    fun  Query(
        projection:Array<String>, selection: String?, selectionArgs: Array<String>?,
        sorOrder:String, dbTable: String):Cursor{
        val qb=SQLiteQueryBuilder()
        qb.tables = dbTable
        return qb.query(sqlDB,projection,selection,selectionArgs,null,null,sorOrder)
    }

    fun Delete(selection:String, selectionArgs: Array<String>, dbTable: String):Long{
        val count = sqlDB!!.delete(dbTable,selection,selectionArgs)
        return  count.toLong()
    }

    fun Update(values:ContentValues,selection:String,selectionargs:Array<String>, dbTable: String):Long{
        val count = sqlDB!!.update(dbTable,values,selection,selectionargs)
        sqlDB!!.close();
        return count.toLong()
    }

}