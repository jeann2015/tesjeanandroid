package com.hntestmobile.mytestjean.adapter

class MovieModel(title: String?, genre: String?, year: String?,ID: Int) {
    private var title: String
    private var genre: String
    private var year: String
    private var id: Int
    init {
        this.title = title!!
        this.genre = genre!!
        this.year = year!!
        this.id = ID!!
    }
    fun getTitle(): String? {
        return title
    }
    fun setTitle(name: String?) {
        title = name!!
    }
    fun getYear(): String? {
        return year
    }
    fun setYear(year: String?) {
        this.year = year!!
    }
    fun getGenre(): String? {
        return genre
    }
    fun setGenre(genre: String?) {
        this.genre = genre!!
    }
    fun getID(): Int? {
        return id
    }
    fun setID(id: Int) {
        this.id = id!!
    }
}